import { Ref, ref } from 'vue';
import IResponse from '@/interfaces/IResponse';
import IError from '@/interfaces/IError';

export class FileService {
    private response:Ref<IResponse>;

    constructor () {
        this.response = ref<IResponse>({});
    }

    getSystem(): Ref<IResponse | IError> {
        return this.response;
    }

    async fetchPath(path?: string): Promise<void> {
        try {
            // const BASE_URL = 'http://localhost:3000';
            let url = process.env.VUE_APP_REST_API;
            if(path) {
                url += path;
            }
            const response = await fetch(url);
            const json = await response.json();
            this.response.value = await json;
        } catch (error) {
            console.log(error)
        }
    }
}