require('dotenv').config();

const express = require('express');
const cors = require('cors');

const router = require('./routes/router');

const app = express()
const port = process.env.PORT || 3001;

app.use(cors({
    origin: '*'
}));

// app.use(fileUpload());
app.use('', router);

app.listen(port, () => console.log(`App listening on http://localhost:${port}`));
