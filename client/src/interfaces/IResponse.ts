import IDirectory from "./IDirectory";

export default interface IResponse {
    path?: string,
    content?: IDirectory
}
