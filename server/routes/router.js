require('dotenv').config();

const router = require('express').Router();
const path = require('path');
const fs = require('fs');
const fileUpload = require('express-fileupload');
const root = process.env.ROOT_STORE;

router.use(fileUpload());

const processPath = (path) => {
    return path ? path.replace(/-/g, '/') : '';
};

router.get('/api/:path(*)?', (req, res) => {
    const content = { folders: [], files: [] };
    const directory = processPath(req.params.path).replaceAll('//', '/');
    console.log(root+directory);
    let files = null;
    try {
        files = fs.readdirSync(root+directory).forEach((file) => {
            if(fs.lstatSync(root+directory + '/'+file).isDirectory()) {
                content.folders.push(file);
            } else {
                content.files.push(file);
            }
        })
    } catch (e) {
        console.log('e.code', e.code);
        if(e.code === process.env.ERROR_NOT_DIR){
            console.log('download ', root+directory);
            return res.download(root+directory);
        } else {
            return res.send({
                code: process.env.ERROR_NOT_FOUND,
                message: 'No se encontró el directorio',
                error: e
            });
        }
    }

    res.send({ 
        path: (root+directory).replace(process.env.ROOT_STORE, '/root/'),
        content
    });
});

router.post('/api/:path(*)?', async (req, res) => {
    console.log(req.params.path);
    // console.log('files ', req.files);
    console.log('files.uploads ', req.files.uploads);
    const directory = root + processPath(req.params.path);
    let sampleFile;
    let uploadPath;
    let uploads = [];

    if (!req.files || Object.keys(req.files).length === 0) {
        return res.status(400).send('No files were uploaded.');
    }


    // TODO grabar en un root OK
    // TODO grabar en un path OK
    // TODO grabar un solo archivo
    // TODO grabar en un nuevo path
    // TODO pisar y forzar archivos

    // The name of the input field (i.e. "sampleFile") is used to retrieve the uploaded file
    Promise.all(
        req.files.uploads.map( file => {
            sampleFile = file;
            uploadPath = directory + '/' + sampleFile.name;
            console.log(uploadPath);
            // Use the mv() method to place the file somewhere on your server
            return new Promise((resolve, reject) => {
                sampleFile.mv(uploadPath, function(err) {
                    if (err) reject(err);

                    uploads.push(sampleFile.name);
                    resolve();
                });
            })
        })
        ).then(files => {
            res.send({ message: 'Archivos subidos correctamente', path: directory, uploads }) 
        })
        .catch(err => {
            return res.status(500).send(err);
        })
        
});

// TODO eliminar archivo
// router.delete('/api/:path?', (req, res) => {
    
// });

module.exports = router;