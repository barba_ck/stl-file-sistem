import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
import ContentView from '../views/ContentView.vue'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/:pathResource*',
    name: 'home',
    alias: ['/'],
    component: ContentView
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
