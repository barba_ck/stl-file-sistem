export default interface IDirectory {
    folders: string[],
    files: string[]
}